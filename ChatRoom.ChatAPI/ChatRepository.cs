﻿﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ChatRoom.Model;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;
using System.Linq;

namespace ChatRoom.ChatAPI
{
    #region interface

    public interface IChatRepository
    {
        Task<List<ChatMessage>> GetMessages();
        Task AddMessage(ChatMessage message);
    }
    #endregion

    public class ChatRepository: IChatRepository
    {
        private readonly string _chatTableName;
        private readonly CloudTableClient _chatClient;

        #region constructor
        //connection to azure table storage
        public ChatRepository(string groupName)
        {            
            var accountName = Environment.GetEnvironmentVariable("AZURE_STORAGE_ACCOUNT_NAME");
            var accountKey = Environment.GetEnvironmentVariable("AZURE_STORAGE_ACCOUNT_KEY"); 
            _chatTableName = groupName;

            var storageCredentials = new StorageCredentials(accountName, accountKey);
            var storageAccount = new CloudStorageAccount(storageCredentials, true);
            _chatClient = storageAccount.CreateCloudTableClient();
        }
        #endregion

        #region fetches chat history. last 50 in this case

        public async Task<List<ChatMessage>> GetMessages()
        {
            var table = _chatClient.GetTableReference(_chatTableName);

            // Create the table if it doesn't exist.
            await table.CreateIfNotExistsAsync();

            string filter = TableQuery.GenerateFilterCondition(
                "PartitionKey",
                QueryComparisons.Equal,
                _chatTableName);

            var query = new TableQuery<ChatMessageEntity>()
                .Where(filter)
                .Take(50);

            var entities = await table.ExecuteQuerySegmentedAsync(query, null);

            var result = entities.Results.Select(entity =>
                new ChatMessage
                {
                    Id = entity.RowKey,
                    Message = entity.Message,
                    Sender = entity.Sender,
                    SentDate = entity.SentDate
                }).OrderBy(m => m.SentDate).ToList();

            return result;
        }
        #endregion

        #region stores chat messages
        //store chat messages to azure table storage
        public async Task AddMessage(ChatMessage message)
        {
            var table = _chatClient.GetTableReference(_chatTableName);

            await table.CreateIfNotExistsAsync();

            var chatMessage = new ChatMessageEntity(message.GroupName, message.Id)
            {
                Message = message.Message,
                Sender = message.Sender,
                SentDate = DateTime.Now
            };

            TableOperation insertOperation = TableOperation.Insert(chatMessage);

            await table.ExecuteAsync(insertOperation);
        }
        #endregion

    }
}
