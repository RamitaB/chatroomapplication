﻿using System;
using ChatRoom.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatRoom.ChatAPI
{
    #region interface
    public interface IChatService
    {
        Task<List<ChatMessage>> GetMessages();
        Task AddMessage(string groupName, string senderName, string message);
    }
    #endregion

    public class ChatService: IChatService
    {
        private readonly IChatRepository _repository;

        #region constructor
        public ChatService(string groupName)
        {
            _repository = new ChatRepository(groupName);

        }
        #endregion

        #region chat history fetch
        
        //fetches the chat history from db
        public async Task<List<ChatMessage>> GetMessages()
        {
            return await _repository.GetMessages();
        }
        #endregion

        #region store chat message

        //adds the chat history to db
        public async Task AddMessage(string groupName, string senderName, string message)
        {
            var chatMessage = new ChatMessage()
            {
                Id = Guid.NewGuid().ToString(),
                Sender = senderName,
                Message = message,
                GroupName = groupName
            };
            await _repository.AddMessage(chatMessage);
        }
        #endregion

    }
}
