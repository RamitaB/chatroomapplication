﻿using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace ChatRoom.ChatAPI
{
    //table entity for chat messages
    public class ChatMessageEntity:TableEntity
    {
        //partition key is group name and row key is message id
        public ChatMessageEntity(string groupName,string messageId)
        {
            PartitionKey = groupName;
            RowKey = messageId;
        }
        public ChatMessageEntity() { }
        public string Sender { get; set; }
        public DateTime SentDate { get; set; }
        public string Message { get; set; }
    }
}
