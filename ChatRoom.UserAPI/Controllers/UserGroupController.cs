﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ChatRoom.UserAPI.Repository;
using ChatRoom.Model;

namespace ChatRoom.UserAPI.Controllers
{
    #region interface
    public interface IUserGroupController
    {
        List<UserGroupViewModel> GetUserGroup(string name);
        Task<bool> AddUserGroup(string groupName, string userName);
        void DeleteUserGroup(string id);
    }
    #endregion

    [ApiController]
    [Route("[controller]")]
    public class UserGroupController : ControllerBase, IUserGroupController
    {
        private readonly ApplicationDbContext db;
        private readonly IUserGroupService userGroupService;

        #region constructor
        public UserGroupController()
        {
            this.db = new ApplicationDbContext();
            this.userGroupService = new UserGroupService(db);
        }
        #endregion

        #region GetUserGroup
        /// <summary>
        /// get the group list which user has already joined
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet("getUserGroup/{userName}")]
        public List<UserGroupViewModel> GetUserGroup(string userName)
        {
             var userGroup = userGroupService.GetUserGroup(userName);
             return userGroup;
        }
        #endregion

        #region AddUserGroup

        /// <summary>
        /// Adds new group and map it to user
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<bool> AddUserGroup(string groupName, string userName)
        {
            return await userGroupService.AddUserGroup(groupName, userName);
        }
        #endregion

        #region DeleteUserGroup
        /// <summary>
        /// deletes the user group mapping once user leaves the group
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void DeleteUserGroup(string id)
        {
            userGroupService.DeleteUserGroup(id);
        }
        #endregion
    }
}
