﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ChatRoom.UserAPI.Service;

namespace ChatRoom.UserAPI.Controllers
{
    #region interface
    public interface IUserController
    {
        string Login(string userName);
    }
    #endregion

    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ApplicationDbContext db;
        private readonly IUserService userService;

        #region constructor
        public UserController()
        {
            this.db = new ApplicationDbContext();
            this.userService = new UserService(db);
        }
        #endregion

        #region Login
        /// <summary>
        /// If user already exists return the user else creates one
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet("login/{userName}")]
        public string Login(string userName)
        {            
            return userService.Login(userName);
        }
        #endregion
    }
}
