﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ChatRoom.UserAPI.Service;
using ChatRoom.Model;

namespace ChatRoom.UserAPI.Controllers
{
    #region interface
    public interface IGroupController
    {
        Task AddGroup(string groupName, string userName);
        List<Group> GetOtherGroup(string name);
    }
    #endregion

    [ApiController]
    [Route("[controller]")]
    public class GroupController : ControllerBase, IGroupController
    {
        private readonly ApplicationDbContext db;
        private readonly IGroupService groupService;

        #region constructor
        public GroupController()
        {
            this.db = new ApplicationDbContext();
            this.groupService = new GroupService(db);
        }
        #endregion

        #region AddGroup

        /// <summary>
        /// Adds new group and map it to user
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost("AddGroup")]
        public async Task AddGroup(string groupName, string userName)
        {
            await groupService.AddGroup(groupName, userName);
        }
        #endregion

        #region GetOtherGroup

        //get the group list which user has not joined
        [HttpGet("getOtherGroup/{userName}")]
        public List<Group> GetOtherGroup(string userName)
        {
            var userGroup = groupService.GetOtherGroup(userName);
            return userGroup;
        }
        #endregion
    }
}
