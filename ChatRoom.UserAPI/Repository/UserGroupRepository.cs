﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChatRoom.Model;
using Microsoft.EntityFrameworkCore;

namespace ChatRoom.UserAPI.Service
{
    #region interface
    public interface IUserGroupRepository
    {
        List<UserGroupViewModel> GetUserGroup(string name);
        Task<bool> AddUserGroup(string groupName, string userName);
        void DeleteUserGroup(string id);
    }
    #endregion

    public class UserGroupRepository : IUserGroupRepository
    {
        private ApplicationDbContext context;

        #region constructor
        public UserGroupRepository(ApplicationDbContext _context)
        {
            context = _context;
        }
        #endregion

        #region GetUserGroup
        /// <summary>
        /// gets the list of group which user has joined already
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<UserGroupViewModel> GetUserGroup(string name)
        {
            var userGroups = context.UserGroupViewModels.FromSqlRaw("exec usp_GetUserGroups {0}", name).ToList();
            var userGroupList = userGroups.Any(u => u.Id == -1) == false ? userGroups : new List<UserGroupViewModel>();
            return userGroupList;
        }
        #endregion

        #region AddUserGroup
        /// <summary>
        /// maps a user to group if that user count in that group doesnot exceeds 4
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public async Task<bool> AddUserGroup(string groupName, string userName)
        {
            int userCount =Convert.ToInt32(Environment.GetEnvironmentVariable("USER_LIMIT_IN_GROUP"));
            int groupId = context.Groups.FirstOrDefault(g => g.Name == groupName).Id;
            if (GetUserCountInGroup(groupId) > userCount)
            {
                return false;
            }

            int userId = context.Users.FirstOrDefault(g => g.Name == userName).Id;
            var userGroup = new UserGroup
            {
                GroupId = groupId,
                UserId = userId
            };
            context.UserGroups.Add(userGroup);
            await context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// gets the user count in a group
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        private int GetUserCountInGroup(int groupId)
        {
            var count = context.UserGroups.Count(ug => ug.GroupId == groupId);
            return count;

        }
        #endregion

        #region DeleteUserGroup
        /// <summary>
        /// deletes the user to group map when user leaves a group
        /// </summary>
        /// <param name="id"></param>
        public void DeleteUserGroup(string id)
        {
            int groupId = Convert.ToInt32(id);
            var userGroup = context.UserGroups.FirstOrDefault(x => x.Id == groupId);
            if (userGroup != null)
            {
                context.UserGroups.Remove(userGroup);
                context.SaveChanges();
            }

        }
        #endregion

    }
}
