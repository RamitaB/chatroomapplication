﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChatRoom.Model;
using Microsoft.EntityFrameworkCore;

namespace ChatRoom.UserAPI.Repository
{
    #region interface
    public interface IGroupRepository
    {
        Task AddGroup(string groupName, string userName);
        List<Group> GetOtherGroup(string name);
    }
    #endregion

    public class GroupRepository : IGroupRepository
    {
        private ApplicationDbContext context;

        #region constructor
        public GroupRepository(ApplicationDbContext _context)
        {
            context = _context;
        }
        #endregion

        #region AddGroup

        /// <summary>
        /// Adds new group and map it to user
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task AddGroup(string groupName, string userName)
        {
            //adds a new group
            DateTime current = DateTime.Now;
            var group = new Group
            {
                Name = groupName,
                InsertDate = current.Date
            };
            context.Groups.Add(group);
            context.SaveChanges();

            //maps the added group to user
            int groupId = context.Groups.FirstOrDefault(g => g.Name == groupName).Id;
            int userId = context.Users.FirstOrDefault(g => g.Name == userName).Id;
            var userGroup = new UserGroup
            {
                GroupId = groupId,
                UserId = userId
            };
            context.UserGroups.Add(userGroup);
            await context.SaveChangesAsync();
        }
        #endregion

        #region GetOtherGroup

        //get the group list which user has not joined
        public List<Group> GetOtherGroup(string name)
        {
            var otherUserGroups = context.Groups.FromSqlRaw("exec usp_GetOtherGroups {0}", name).ToList();
            return otherUserGroups;
        }
        #endregion

    }
}
