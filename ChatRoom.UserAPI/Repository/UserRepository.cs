﻿using System;
using System.Linq;
using ChatRoom.Model;

namespace ChatRoom.UserAPI.Repository
{
    #region interface
    public interface IUserRepository
    {
        User AddUser(string name);
        User GetUser(string name);
    }
    #endregion

    public class UserRepository : IUserRepository
    {
        private ApplicationDbContext context;

        #region constructor
        public UserRepository(ApplicationDbContext _context)
        {
            context = _context;
        }
        #endregion

        #region AddUser

        /// <summary>
        /// Adds a user in the database when new user logs in
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public User AddUser(string name)
        {
            DateTime current = DateTime.Now;
            var user = new User
            {
                Name = name,
                InsertDate = current.Date
            };
            context.Users.Add(user);
            context.SaveChanges();
            return user;
        }
        #endregion

        #region GetUser

        /// <summary>
        /// get user by user name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>        
        public User GetUser(string name)
        {
            var user = context.Users.ToList().Find(user => user.Name == name);
            return user;
        }
        #endregion

    }
}
