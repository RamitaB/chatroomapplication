﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChatRoom.UserAPI.Repository;
using ChatRoom.Model;

namespace ChatRoom.UserAPI.Service
{
    #region interface
    public interface IGroupService
    {
        Task AddGroup(string groupName, string userName);
        List<Group> GetOtherGroup(string name);
    }
    #endregion

    public class GroupService: IGroupService
    {

        private readonly GroupRepository group;

        #region constructor
        public GroupService(ApplicationDbContext db)
        {
            group = new GroupRepository(db);
        }
        #endregion

        #region AddGroup

        /// <summary>
        /// Adds new group and map it to user
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task AddGroup(string groupName, string userName)
        {
            await group.AddGroup(groupName, userName);
            
        }
        #endregion

        #region GetOtherGroup

        //get the group list which user has not joined
        public List<Group> GetOtherGroup(string userName)
        {
            var userGroup = group.GetOtherGroup(userName);
            return userGroup;
        }
        #endregion
    }
}
