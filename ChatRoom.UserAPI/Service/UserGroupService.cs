﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ChatRoom.UserAPI.Service;
using ChatRoom.Model;

namespace ChatRoom.UserAPI.Repository
{
    #region interface
    public interface IUserGroupService
    {
        List<UserGroupViewModel> GetUserGroup(string name);
        Task<bool> AddUserGroup(string groupName, string userName);
        void DeleteUserGroup(string id);

    }
    #endregion

    public class UserGroupService : IUserGroupService
    {        
        private readonly IUserGroupRepository userGroup;

        #region constructor
        public UserGroupService(ApplicationDbContext db)
        {
            userGroup = new UserGroupRepository(db); ;
        }
        #endregion

        #region AddUserGroup
        /// <summary>
        /// maps a user to group if that user count in that group doesnot exceeds 4
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public List<UserGroupViewModel> GetUserGroup(string userName)
        {
            var userGroupList = userGroup.GetUserGroup(userName);
            return userGroupList;
        }
        #endregion

        #region AddUserGroup
        /// <summary>
        /// maps a user to group if that user count in that group doesnot exceeds 4
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public async Task<bool> AddUserGroup(string groupName, string userName)
        {
            return await userGroup.AddUserGroup(groupName, userName);
        }
        #endregion

        #region DeleteUserGroup
        /// <summary>
        /// deletes the user to group map when user leaves a group
        /// </summary>
        /// <param name="id"></param>
        public void DeleteUserGroup(string id)
        {
            userGroup.DeleteUserGroup(id);
        }
        #endregion

    }
}
