﻿using ChatRoom.UserAPI.Repository;
using ChatRoom.Model;

namespace ChatRoom.UserAPI.Service
{
    #region interface
    public interface IUserService
    {
        string Login(string userName);
    }
    #endregion

    public class UserService: IUserService
    {
        private readonly IUserRepository user;

        #region constructor
        public UserService(ApplicationDbContext db)
        {
            user = new UserRepository(db); ;
        }
        #endregion

        #region Login
        /// <summary>
        /// If user already exists return the user else creates one
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public string Login(string userName)
        {
            var userExist = user.GetUser(userName);
            User userObj = userExist == null ? user.AddUser(userName) : userExist;
            return userObj.Name;
        }
        #endregion

    }
}
