﻿using System;

namespace ChatRoom.Model
{
    //this is the model file of the chat messages
    public class ChatMessage
    {
        public string Id { get; set; }
        public string Sender { get; set; }
        public string GroupName { get; set; }
        public DateTime SentDate { get; set; }

        public string Message { get; set; }

    }
}
