﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChatRoom.Model
{
    //this is a view model for user group
    public class UserGroupViewModel
    {
        public Int32 Id { get; set; }

        public Int32 UserId { get; set; }

        public Int32 GroupId { get; set; }
        public string UserName { get; set; }
        public string GroupName { get; set; }

    }
}
