﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace ChatRoom.Model
{
    //this is the model file of the chat groups
    [Table("Group")]
    public class Group
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("Id")]
        public Int32 Id { get; set; }

        [Required]
        public string Name { get; set; }
       
        public DateTime InsertDate { get; set; }
    }
}
