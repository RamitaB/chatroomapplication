﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;


namespace ChatRoom.Model
{
    //this is the mapping model file of user and group
    [Table("UserGroup")]
    public class UserGroup
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("Id")]
        public Int32 Id { get; set; }

        [Required]
        [ForeignKey("FK_UserGroup_User")]
        public Int32 UserId { get; set; }

        [Required]
        [ForeignKey("FK_UserGroup_Group")]
        public Int32 GroupId { get; set; }

    }
}
