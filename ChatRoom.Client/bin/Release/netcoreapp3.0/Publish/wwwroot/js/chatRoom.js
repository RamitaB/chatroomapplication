﻿"use strict";
function appViewModel() {
    var self = this;

    self.userName = ko.observable(null);
    self.groupName = ko.observable(null);
    self.newGroupName = ko.observable(null);

    self.userGroupList = ko.observableArray(null);
    self.otherGroupList = ko.observableArray(null);

    self.visibleChat = ko.observable(false);
    self.visibleAddGroup = ko.observable(false);

    self.chatMessage = ko.observable(null);


    const url = "http://chatroomuserapi.azurewebsites.net/WeatherForecast";

    self.getUserGroupList = function () {
        var userName = location.search.split('?')[1];
        self.userName(userName);
        $.ajax({
            type: 'GET',
            url: 'https://chatroomuserapi.azurewebsites.net/usergroup/getusergroup/' + userName,
            contentType: 'application/json; charset=utf-8',
        })
        .done(function (data) {
            if (data != null) {
                self.userGroupList(data);
                self.userGroupList();
            }           
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
        })
        .always();
    }

    self.getOtherGroupList = function () {
        debugger;
        var userName = location.search.split('?')[1];
        self.userName(userName);
        $.ajax({
            type: 'GET',
            url: 'https://chatroomuserapi.azurewebsites.net/group/getothergroup/' + userName,
            contentType: 'application/json; charset=utf-8',
        })
            .done(function (data) {
                debugger;
                if (data != null) {
                    self.otherGroupList(data);
                    self.userGroupList();
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                debugger;
            })
            .always();
    }

    self.populateChatRoom = function () {
        self.userName();
        self.getUserGroupList();
        self.getOtherGroupList();
        self.visibleAddGroup(false);
        self.userName();
    }

    self.populateChatRoom();

    self.setVisibility = function () {
        self.visibleAddGroup(true);
    }

    

    self.addGroup = function () {
        var groupName = self.newGroupName();
        if (groupName == "" || groupName == null) {
            alert("Please enter a name.");
            return;
        }  
        self.groupName(groupName);
        $.ajax({
            type: 'GET',
            url: 'https://chatroomuserapi.azurewebsites.net/group/addGroup/' + groupName,
            contentType: 'application/json; charset=utf-8',
        })
            .done(function (data) {
                if (data != null) {
                    self.visibleAddGroup(false);
                    self.populateChatRoom();
                    self.newGroupName(null);
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
            })
            .always();
    }

    self.joinGroup = function (target) {
        debugger
        var userName = self.userName();
        var groupName = target.name;
        self.groupName(groupName);

        connection.invoke("JoinGroup", groupName, userName).catch(function (err) {
            return console.error(err.toString());
        });
        self.visibleAddGroup(false);
        self.populateChatRoom();
        event.preventDefault();
    }

       

    var connection = new signalR.HubConnectionBuilder()
        .withUrl("https://chatwebapp.azurewebsites.net/chat", {
            transport: signalR.HttpTransportType.WebSockets
        })
        .build();


    connection.start().catch(function (err) {
        return console.error(err.toString());
    });

    self.checkIfConnected = function () {
        debugger;
        if (connection.connectionState==4) {
            connection.start().catch(function (err) {
                return console.error(err.toString());
            });
        }
    }
    
    connection.on("ReceiveMessage", function (message,user) {
        var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        var div = document.createElement("div");
        div.innerHTML = user+":"+ msg + "<hr/>";
        document.getElementById("messages").appendChild(div);
    });

    document.getElementById("btnSend").addEventListener("click", function (event) {
        debugger;
        var groupName = self.groupName();
        var messageText = self.chatMessage();
        self.chatMessage(null);
        var userName = self.userName();
        var fullMessage = { Msg: messageText, Group: groupName, User: userName };
        connection.invoke("SendMessageToGroup", fullMessage).catch(function (err) {
            return console.error(err.toString());
        });
        event.preventDefault();
    });

    document.getElementById("btnAddGroup").addEventListener("click", function (event) {
        debugger;
        var userName = self.userName();
        var groupName = self.newGroupName();
        if (groupName == "" || groupName == null) {
            alert("Please enter a name.");
            return;
        }
        self.groupName(groupName);
        connection.invoke("JoinRoom", groupName, userName)
            .catch(function (err) {
                return console.error(err.toString()
                );
        });
        self.newGroupName(null);
        self.visibleAddGroup(false);
        self.populateChatRoom();
        event.preventDefault();
    });

    self.startChat = function (target) {
        debugger;
        self.checkIfConnected();
        var userName = self.userName();
        var groupName = target.name;
        self.groupName(groupName);
        debugger;
        connection.invoke("JoinRoom", groupName, userName).catch(function (err) {
            return console.error(err.toString());
        });
        self.groupName(target.groupName);
        self.visibleChat(true);
    }

    self.backToGroup = function () {
        self.visibleChat(false);
        self.chatMessage(null);
        self.populateChatRoom();
    }

    self.leaveGroup = function (target) {
        debugger;
        var id = target.id;
        var groupName = target.groupName;
        var userName = self.userName();
        //connection.invoke("JoinRoom", groupName, userName).catch(function (err) {
        //    return console.error(err.toString());
        //});
        connection.invoke("LeaveRoom", id,groupName).catch(function (err) {
            return console.error(err.toString());
        });
        self.populateChatRoom();

        //$.ajax({
        //    type: 'DELETE',
        //    url: 'https://chatroomuserapi.azurewebsites.net/usergroup/deleteUserGroup/' + id,
        //    contentType: 'application/json'
        //})
        //    .done(function (data) {
        //        if (data != null) {
        //            self.populateChatRoom();
        //        }
        //    })
        //    .fail(function (jqXHR, textStatus, errorThrown) {
        //    })
        //    .always();
    }

}

$(document).ready(function () {
    /* Disable caching that is an ajax problem (ofcourse) with different versions of IE browser. */
    $.ajaxSetup({ cache: false });
    ko.applyBindings(new appViewModel(), document.getElementById('divMain'));
});


