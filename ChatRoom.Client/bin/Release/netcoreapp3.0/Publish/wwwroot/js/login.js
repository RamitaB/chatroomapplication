﻿function appViewModel() {
    var self = this;
    self.userName = ko.observable(null);
    const url = "http://chatroomuserapi.azurewebsites.net/WeatherForecast";

    self.test = function () {
        var userName = self.userName();
        if (userName == "" || userName == null) {
            alert("Please enter a name.");
            return;
        }
        $.ajax({
            type: 'GET',
            url: 'http://chatroomuserapi.azurewebsites.net/user/login/'+ userName,
            contentType: 'application/json; charset=utf-8',
        })
            .done(function (data) {
            window.location.href = "http://localhost:59332/chatroom?" + userName;
        })
            .fail(function (jqXHR, textStatus, errorThrown) {
        })
        .always();
    }
}

$(document).ready(function () {
    /* Disable caching that is an ajax problem (ofcourse) with different versions of IE browser. */
    $.ajaxSetup({ cache: false });
    ko.applyBindings(new appViewModel(), document.getElementById('testdiv'));

});


