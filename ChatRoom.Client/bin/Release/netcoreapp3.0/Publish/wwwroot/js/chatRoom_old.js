﻿"use strict";

var connection = new signalR.HubConnectionBuilder()
    .withUrl("https://chatwebapp.azurewebsites.net/chat", {
        transport: signalR.HttpTransportType.WebSockets
    })
    .build();
debugger;

var i = document.getElementById("divBody");
var t = document.getElementById("messages");

connection.on("ReceiveMessage", function (message) {
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var div = document.createElement("div");
    div.innerHTML = msg + "<hr/>";
    document.getElementById("messages").appendChild(div);
});

connection.start().catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    debugger;
    var message = document.getElementById("message").value;
    connection.invoke("SendMessageToAll", message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});


//document.getElementById("addGroup").addEventListener("click", function (event) {
//    debugger;
//    var message = document.getElementById("message").value;
//    connection.invoke("JoinRoom", message).catch(function (err) {
//        return console.error(err.toString());
//    });
//    event.preventDefault();
//});

//document.getElementById("removeGroup").addEventListener("click", function (event) {
//    debugger;
//    var message = document.getElementById("message").value;
//    connection.invoke("LeaveRoom", message).catch(function (err) {
//        return console.error(err.toString());
//    });
//    event.preventDefault();
//});