﻿function appViewModel() {
    var self = this;
    self.userName = ko.observable(null);

    self.test = function () {
        debugger;
        var userName = self.userName();
        if (userName == "" || userName == null) {
            alert("Please enter a name.");
            return;
        }
        $.ajax({
            type: 'GET',
            url: 'https://chatroomuserapi.azurewebsites.net/user/login/'+ userName,
            contentType: 'application/json; charset=utf-8',
        })
            .done(function (data) {
                window.location.href = "https://chatclientapp.azurewebsites.net/chatroom?" + userName;
        })
            .fail(function (jqXHR, textStatus, errorThrown) {
        })
        .always();
    }
}

$(document).ready(function () {
    /* Disable caching that is an ajax problem (ofcourse) with different versions of IE browser. */
    $.ajaxSetup({ cache: false });
    ko.applyBindings(new appViewModel(), document.getElementById('testdiv'));

});


