﻿"use strict";
function appViewModel() {
    var self = this;

    //===========variable start==========//
    const userUrl = "https://chatroomuserapi.azurewebsites.net/";
    const hubUrl = "https://chatwebapp.azurewebsites.net/";
    //===========variable start==========//


    //===========observables start==========//

    self.userName = ko.observable(null);
    self.groupName = ko.observable(null);
    self.newGroupName = ko.observable(null);

    self.userGroupList = ko.observableArray(null);
    self.otherGroupList = ko.observableArray(null);

    self.visibleChat = ko.observable(false);
    self.visibleAddGroup = ko.observable(false);

    self.chatMessage = ko.observable(null);
    self.messageHistory = ko.observableArray();

    //===========observables ends==================//


    //=======================user api call starts============================//

    //fetch the all the groups which user has already joined
    self.getUserGroupList = function () {
        var userName = location.search.split('?')[1];
        self.userName(userName);
        $.ajax({
            type: 'GET',
            url: userUrl+'usergroup/getusergroup/' + userName,
            contentType: 'application/json; charset=utf-8',
        })
            .done(function (data) {
                if (data != null) {
                    self.userGroupList(data);
                    self.userGroupList();
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
            })
            .always();
    }

    //fetch the all the groups which user has not joined. created by other users
    self.getOtherGroupList = function () {
        var userName = location.search.split('?')[1];
        self.userName(userName);
        $.ajax({
            type: 'GET',
            url: userUrl +'group/getothergroup/' + userName,
            contentType: 'application/json; charset=utf-8',
        })
            .done(function (data) {
                if (data != null) {
                    self.otherGroupList(data);
                    self.userGroupList();
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
            })
            .always();
    }

    //when a user joins a existing group, created by other user
    self.joinGroup = function (target) {
        var userName = self.userName();
        var groupName = target.name;
        self.groupName(groupName);

        connection.invoke("JoinGroup", groupName, userName)
            .then(function (result) {
                if (result == false) {
                    alert('User count in ' + groupName + ' exceeds 4!!!');
                    return;
                }
                self.visibleAddGroup(false);
                self.populateChatRoom();
                event.preventDefault();
            })
            .catch(function (err) {
                return console.error(err.toString());
            });
    }

    //=======================user api call ends============================//

    //=======================page load calls starts============================//

    //populates the user joined groups and other users group 
    self.populateChatRoom = function () {
        self.getUserGroupList();
        self.getOtherGroupList();
        self.visibleAddGroup(false);
    }

    self.populateChatRoom();

    //set the visibility of add group section
    self.setVisibility = function () {
        self.visibleAddGroup(true);
    }

    //back to group page from chat page
    self.backToGroup = function () {
        self.visibleChat(false);
        self.chatMessage(null);
        self.populateChatRoom();
    }
    //=======================page load calls ends============================//

   //=======================hub connection starts============================//

    //creates a connection builder
    var connection = new signalR.HubConnectionBuilder()
        .withUrl(hubUrl+"chat", {
            transport: signalR.HttpTransportType.WebSockets
        })
        .build();

    //strats the connection
    connection.start().catch(function (err) {
        return console.error(err.toString());
    });

    //checks if the connection got disconnected . if so, connects it again
    self.checkIfConnected = function () {
        if (connection.connectionState == 4) {
            connection.start().catch(function (err) {
                return console.error(err.toString());
            });
        }
    }

  //=======================hub connection ends============================//



   //=======================hub method calls starts============================//

    //when a user creates a new group and automatically joins the same
    document.getElementById("btnAddGroup").addEventListener("click", function (event) {
        var userName = self.userName();
        var groupName = self.newGroupName();
        if (groupName == "" || groupName == null) {
            alert("Please enter a name.");
            return;
        }
        self.groupName(groupName);
        self.checkIfConnected();
        connection.invoke("JoinRoom", groupName, userName)
            .then(function () {
                self.newGroupName(null);
                self.visibleAddGroup(false);
                self.populateChatRoom();
                event.preventDefault();
            })
            .catch(function (err) {
                return console.error(err.toString()
                );
            });

    });

    //when user starts the chating in a group. It allso shows the chat history
    self.startChat = function (target) {
        self.visibleChat(true);
        var groupName = target.groupName;
        self.groupName(groupName);
        document.getElementById("messages").innerHTML = '';
        self.checkIfConnected();
        connection.invoke("GetChatHistory", groupName)
            .then(function (result) {
                self.messageHistory.removeAll();
                self.messageHistory(result);
                self.messageHistory();
            })
            .catch(function (err) {
                return console.error(err.toString());
            });
    }

    //when user leaves the group
    self.leaveGroup = function (target) {
        var id = target.id.toString();
        var groupName = target.groupName;
        self.checkIfConnected();
        connection.invoke("LeaveRoom", id, groupName)
            .then(function (result) {
                self.populateChatRoom();
            })
            .catch(function (err) {
                return console.error(err.toString());
            });
    }


    //receives the chat messages from hub 
    connection.on("ReceiveMessage", function (message, user) {
        var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        var div = document.createElement("div");
        div.innerHTML = user + " : " + msg + "<hr/>";
        document.getElementById("messages").appendChild(div);
    });


    //sends the chat messages to hub for broadcasting to other users
    document.getElementById("btnSend").addEventListener("click", function (event) {
        var groupName = self.groupName();
        var messageText = self.chatMessage();
        self.chatMessage(null);
        var userName = self.userName();
        var fullMessage = { Message: messageText, GroupName: groupName, Sender: userName };
        self.checkIfConnected();
        connection.invoke("SendMessageToGroup", fullMessage).catch(function (err) {
            return console.error(err.toString());
        });
        event.preventDefault();
    });

   //=======================hub method calls ends============================//
}

$(document).ready(function () {
    /* Disable caching that is an ajax problem (ofcourse) with different versions of IE browser. */
    $.ajaxSetup({ cache: false });
    ko.applyBindings(new appViewModel(), document.getElementById('divMain'));
});


