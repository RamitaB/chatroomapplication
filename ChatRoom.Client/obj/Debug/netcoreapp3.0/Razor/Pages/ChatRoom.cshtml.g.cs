#pragma checksum "C:\Ramita\Personal\Interview test\Next games\ChatRoomApplication\ChatRoom.Client\Pages\ChatRoom.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a45e3cddc3ce87da8f466060840a8598d8851da1"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(ChatRoom.Client.Pages.Pages_ChatRoom), @"mvc.1.0.razor-page", @"/Pages/ChatRoom.cshtml")]
namespace ChatRoom.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Ramita\Personal\Interview test\Next games\ChatRoomApplication\ChatRoom.Client\Pages\_ViewImports.cshtml"
using ChatRoom.Client;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a45e3cddc3ce87da8f466060840a8598d8851da1", @"/Pages/ChatRoom.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d8719ff9ca7b3e7753b3c151d81da4a4c8ff81d4", @"/Pages/_ViewImports.cshtml")]
    public class Pages_ChatRoom : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/chatRoom.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/signalr.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", new global::Microsoft.AspNetCore.Html.HtmlString("text/javascript"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/knockout/knockout-min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/jquery/dist/jquery.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "C:\Ramita\Personal\Interview test\Next games\ChatRoomApplication\ChatRoom.Client\Pages\ChatRoom.cshtml"
  
    ViewData["Title"] = "Chat Room";

#line default
#line hidden
#nullable disable
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a45e3cddc3ce87da8f466060840a8598d8851da14958", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a45e3cddc3ce87da8f466060840a8598d8851da15997", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a45e3cddc3ce87da8f466060840a8598d8851da17036", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a45e3cddc3ce87da8f466060840a8598d8851da18158", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"

<div id=""divMain"">
    <div class=""row justify-content-center"" id=""divGroup"" data-bind=""visible: !visibleChat()"">       
        <div class=""form-group text-center col-sm-12 justify-content-center row"">
            <h2 class=""text-center"">Hi <span data-bind=""text: userName""></span>!!! Welcome to your chat room!!!</h2>
        </div>
        <div class=""form-group text-center col-sm-4 border border-primary justify-content-center table-responsive"">            
            <h4 class=""text-center"">Other Groups</h4>
            <input id=""btnLogin"" type=""button"" value=""Create New Group!!"" class=""btn btn-link form-control text-center"" data-bind=""click:setVisibility"" />

");
            WriteLiteral(@"            <table class=""form-group col-sm-4 border border-primary"" data-bind=""visible: visibleAddGroup"">
                <tr>
                    <td>
                        <label class=""control-label"">Group Name</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type=""text"" class=""control-label"" data-bind=""value: newGroupName"">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id=""btnAddGroup"" type=""button"" value=""Add"" class=""btn btn-primary form-control text-center"" />
                    </td>
                </tr>
            </table>
");
            WriteLiteral(@"            <table class=""table table-striped table-condensed"">
                <tbody data-bind=""foreach: otherGroupList"">
                    <tr>
                        <td class=""text-center""><span data-bind=""text: $data.name""></span></td>
                        <td class=""text-left""><input id=""btnJoinGroup"" type=""button"" value=""Join Group"" class=""btn btn-link form-control text-center"" data-bind=""click: $root.joinGroup"" /></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class=""col-sm-2"">
        </div>
        <div class=""form-group text-center col-sm-6 border border-primary justify-content-center table-responsive"">
            <h4 class=""text-center"">Your Groups</h4>
            <table class=""table table-striped table-condensed"">
                <tbody data-bind=""foreach: userGroupList"">
                    <tr>
                        <td class=""text-center""><span data-bind=""text: $data.groupName""></span></td>
                  ");
            WriteLiteral(@"      <td class=""text-left""><input id=""btnStartChat"" type=""button"" value=""Start Chat"" class=""btn btn-link form-control text-center"" data-bind=""click: $root.startChat""/></td>
                        <td class=""text-left""><input id=""btnLogin"" type=""button"" value=""Leave Group"" class=""btn btn-link form-control text-center"" data-bind=""click: $root.leaveGroup"" /></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class=""row justify-content-center"" id=""divChat"" data-bind=""visible: visibleChat()"">
        <div class=""form-group text-center col-sm-12 justify-content-center row"">
            <h2 class=""text-center"">Hi <span data-bind=""text: userName""></span>!!! Wlcome to group <span data-bind=""text: groupName""></span>!!!</h2>
        </div>
");
            WriteLiteral(@"        <input type=""button"" id=""btnBack"" class=""btn btn-link form-control text-left"" value=""Back to Groups"" data-bind=""click: $root.backToGroup"" />
        <div class=""form-group text-center col-sm-6 border border-primary justify-content-center row"">
            <p style=""margin-top:10px"">
                <textarea name=""message"" id=""message"" class=""justify-content-left align-bottom"" data-bind=""value: chatMessage""></textarea>
                <input type=""button"" id=""btnSend"" class="" col-md-5 btn btn-link form-control text-left"" value=""Send Message"" />
            </p>
            <p>
                <table class=""table table-striped table-condensed"">
                    <tbody data-bind=""foreach: messageHistory"">
                        <tr>
                            <td class=""text-center""><span data-bind=""text: $data.sender""></span></td>
                            <td class=""text-center""><span data-bind=""text: $data.message""></span></td>
                        </tr>
                    </t");
            WriteLiteral("body>\r\n                </table>\r\n            </p>\r\n            <p><div class=\"form-group text-center justify-content-center table-responsive\" id=\"messages\"></div></p>\r\n        </div>\r\n    </div>\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IndexModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel>)PageContext?.ViewData;
        public IndexModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
