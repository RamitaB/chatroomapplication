﻿"use strict";

var connection = new signalR.HubConnectionBuilder()
    .withUrl("https://chatwebapp.azurewebsites.net/chat", {
        transport: signalR.HttpTransportType.WebSockets
    })
    .build();

connection.on("ReceiveMessage", function (message) {
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var div = document.createElement("div");
    div.innerHTML = msg + "<hr/>";
    document.getElementById("messages").appendChild(div);
});

connection.start().catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    debugger;
    var message = document.getElementById("message").value;
    connection.invoke("SendMessageToAll", message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});

document.getElementById("btnAdd").addEventListener("click", function (event) {
    debugger;
    var userName = "test1"
    var groupName = document.getElementById("message").value;
    if (groupName == "" || groupName == null) {
        alert("Please enter a name.");
        return;
    }
    connection.invoke("JoinRoom", groupName, userName).catch(function (err) {
        return console.error(err.toString());
    });
    //self.visibleAddGroup(false);
    //self.populateChatRoom();
    //self.groupName(null);
    event.preventDefault();
});


//"use strict";
//function appViewModel() {
//    var self = this;
//    debugger;

//    self.userName = ko.observable(null);
//    self.groupName = ko.observable(null);

//    self.userGroupList = ko.observableArray(null);
//    self.otherGroupList = ko.observableArray(null);

//    self.visibleChat = ko.observable(false);
//    self.visibleAddGroup = ko.observable(false);


//    const url = "http://chatroomuserapi.azurewebsites.net/WeatherForecast";

//    self.getUserGroupList = function () {
//        debugger;
//        var userName = location.search.split('?')[1];
//        self.userName(userName);
//        $.ajax({
//            type: 'GET',
//            url: 'https://chatroomuserapi.azurewebsites.net/usergroup/getusergroup/' + userName,
//            contentType: 'application/json; charset=utf-8',
//        })
//        .done(function (data) {
//            if (data != null) {
//                self.userGroupList(data);
//                self.userGroupList();
//            }           
//        })
//        .fail(function (jqXHR, textStatus, errorThrown) {
//        })
//        .always();
//    }

//    self.getOtherGroupList = function () {
//        debugger;
//        var userName = location.search.split('?')[1];
//        self.userName(userName);
//        $.ajax({
//            type: 'GET',
//            url: 'https://chatroomuserapi.azurewebsites.net/group/getothergroup/' + userName,
//            contentType: 'application/json; charset=utf-8',
//        })
//            .done(function (data) {
//                debugger;
//                if (data != null) {
//                    self.otherGroupList(data);
//                    self.userGroupList();
//                }
//            })
//            .fail(function (jqXHR, textStatus, errorThrown) {
//                debugger;
//            })
//            .always();
//    }

//    self.populateChatRoom = function () {
//        self.getUserGroupList();
//        self.getOtherGroupList();
//        self.visibleAddGroup(false);
//    }

//    self.populateChatRoom();

//    self.setVisibility = function () {
//        self.visibleAddGroup(true);
//    }

//    document.getElementById("btnAdd").addEventListener("click", function (event) {
//        debugger;
//        var userName = self.userName();
//        var groupName = self.groupName();
//        if (groupName == "" || groupName == null) {
//            alert("Please enter a name.");
//            return;
//        }
//        connection.invoke("JoinRoom", groupName, userName).catch(function (err) {
//            return console.error(err.toString());
//        });
//        self.visibleAddGroup(false);
//        self.populateChatRoom();
//        self.groupName(null);
//        event.preventDefault();
//    });

//    self.addGroup = function () {
//        var groupName = self.groupName();
//        if (groupName == "" || groupName == null) {
//            alert("Please enter a name.");
//            return;
//        }       
//        $.ajax({
//            type: 'GET',
//            url: 'https://chatroomuserapi.azurewebsites.net/group/addGroup/' + groupName,
//            contentType: 'application/json; charset=utf-8',
//        })
//            .done(function (data) {
//                if (data != null) {
//                    self.visibleAddGroup(false);
//                    self.populateChatRoom();
//                    self.groupName(null);
//                }
//            })
//            .fail(function (jqXHR, textStatus, errorThrown) {
//            })
//            .always();
//    }

//    self.joinGroup = function (target) {
//        var groupName = target.name;
//        var groupId = target.id;
//        var userName = self.userName();        
//        $.ajax({
//            type: 'POST',
//            url: 'https://chatroomuserapi.azurewebsites.net/usergroup/addUserGroup/',
//            contentType: 'application/json',
//            dataType: 'json',
//            data: ko.toJSON({
//                "Id": 0,
//                "UserId": 0,
//                "GroupId": groupId,
//                "UserName": userName,
//                "GroupName": groupName
//            })
//        })
//            .done(function (data) {
//                if (data != null) {
//                    if (data == false) {
//                        alert("Sorry!!! This group exceeds 4 users!");
//                        return;
//                    }
//                    self.populateChatRoom();
//                }
//            })
//            .fail(function (jqXHR, textStatus, errorThrown) {
//            })
//            .always();
//    }

//    self.leaveGroup = function (target) {
//        var id = target.id;
//        $.ajax({
//            type: 'DELETE',
//            url: 'https://chatroomuserapi.azurewebsites.net/usergroup/deleteUserGroup/' + id ,
//            contentType: 'application/json'
//        })
//            .done(function (data) {
//                if (data != null) {
//                    self.populateChatRoom();
//                }
//            })
//            .fail(function (jqXHR, textStatus, errorThrown) {
//            })
//            .always();
//    }

//    self.startChat = function () {
//        self.visibleChat(true);
//    }

//    var connection = new signalR.HubConnectionBuilder()
//        .withUrl("https://chatwebapp.azurewebsites.net/chat", {
//            transport: signalR.HttpTransportType.WebSockets
//        })
//        .build();
//    debugger;

//    var i = document.getElementById("divBody");
//    var t = document.getElementById("messages");

//    connection.on("ReceiveMessage", function (message) {
//        var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
//        var div = document.createElement("div");
//        div.innerHTML = msg + "<hr/>";
//        document.getElementById("messages").appendChild(div);
//    });

//    connection.start().catch(function (err) {
//        return console.error(err.toString());
//    });

//    document.getElementById("sendButton").addEventListener("click", function (event) {
//        debugger;
//        var message = document.getElementById("message").value;
//        connection.invoke("SendMessageToAll", message).catch(function (err) {
//            return console.error(err.toString());
//        });
//        event.preventDefault();
//    });

//}

//$(document).ready(function () {
//    /* Disable caching that is an ajax problem (ofcourse) with different versions of IE browser. */
//    $.ajaxSetup({ cache: false });
//    ko.applyBindings(new appViewModel(), document.getElementById('divMain'));
//});


