﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using ChatRoom.UserAPI.Controllers;
using ChatRoom.Model;
using ChatRoom.ChatAPI;

namespace ChatRoom.Service.Hubs
{
    public class ChatHub:Hub
    {
        private IChatService chatService;
        private IGroupController groupController;
        private IUserGroupController userGroupController;

        #region send message

        //sends messages to all the users of a group
        public async Task SendMessageToGroup(ChatMessage messageObj)
        {
            string message = messageObj.Message;
            string group = messageObj.GroupName;
            string user = messageObj.Sender;
            chatService = new ChatService(group);
            await chatService.AddMessage(group, user, message);
            await Clients.Group(group).SendAsync("ReceiveMessage", message, user);
        }

        #endregion
        
        #region on connected
        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
        }
        #endregion

        #region on disconnected
        public override async Task OnDisconnectedAsync(Exception exception)
        {
            //await Groups.RemoveFromGroupAsync(Context.ConnectionId, "SignalR Users");
            await base.OnDisconnectedAsync(exception);
        }
        #endregion

        #region create a new room and joins
        /// <summary>
        /// creates a new group and user joins the group
        /// </summary>
        public async Task JoinRoom(string roomName, string userName)
        {
            groupController = new GroupController();
            await groupController.AddGroup(roomName, userName);
        }
        #endregion

        #region join an existing room
        /// <summary>
        /// when user joins already existing group..user count in that group is checked before joining.
        /// </summary>
        public async Task<bool> JoinGroup(string roomName, string userName)
        {
            userGroupController = new UserGroupController();
            bool result=await userGroupController.AddUserGroup(roomName, userName);
            return result;
        }
        #endregion

        #region leaves a room
        public async Task LeaveRoom(string userGroupId, string groupName)
        {
            var userGroup = new UserGroupController();
            userGroup.DeleteUserGroup(userGroupId);
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        }
        #endregion

        #region fetches chat history of a group
        public async Task<List<ChatMessage>> GetChatHistory(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
            chatService = new ChatService(groupName);
            List<ChatMessage>  messages=await chatService.GetMessages();
            return messages;
        }
        #endregion

    }
}
